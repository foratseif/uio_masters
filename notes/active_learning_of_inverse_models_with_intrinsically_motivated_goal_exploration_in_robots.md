## Active learning of inverse models with intrinsically motivated goal exploration in robots

### TEXT


### NOTES

***Self-Adaptive Goal Generation Robust Intelligent Adaptive Curiosity (SAGG-RIAC) is an intinsically motivated goal exploration mechanism which allow active learning of inverse models in high-dimentional redundant robots. ***

**Experiments in three different robotic setups:**

1. Learning the inverse kinematics in a highly-redundant robotic arm
2. Learning omni-directional locomotion with motor primitives in a quadruped robot
3. An arm learning to control a fishing rod with flexible wire

**Findings:**

1. Exploration in the task space can be a lot faster than exploration in the actuator space for learning inverse models in redundant robots
2.  selecting goals maximizing competence progress creates developmental trajectories driving the robot to progressively focus on tasks of increasing complexity and is statically significantly more efficient than selecting tasks randomly. as well as more efficient than different standard active motor babbling methods.
3. this architecture allows the robot to actively discover which parts of its task space it can learn to reach and which part it cannot.

#### 1. Motor learning and exploration of forward and inverse models

 - It is impossible to model a priori (based on old facts) model for a robot that knows how to interract with every human environment.
 - Therefore the learning of these models have to come through experience, this leads to the following:
   1. "these models are often high-dimensional, continuous and highly non-stationary spatially, and sometimes temporally;"
   2. "learning examples have to be collected autonomously and incrementally by robots;"
   3. "learning, [...] , can happen either through self-experimentation or observation, and both these takes significant physical time in the real world." This means that the training examples that can be collected in a life time is not enough witth regard to the complexity of the space.
- This article presents an approach to address constrained exploration and learning of inverse models in robots. This exploration will be based on escentially two priciples:
  1. Active goal/task exploration in a parameterized task space
     - still dont know what that means
  2. Interestingness as empirically evaluated competence progress
     - still dont know what that means


##### 1.1 Constraining the exploration

- A set of constrains must be applied to the exploration process, to maximally reduce the size and/or dimentionality of the exploreable space.
	- One aproach to constraint **Social guidance** is when a human demonstrator assists the robot in its learning.
	  - this prevents the robot from doing its own exploration.
	  - and requires a trainer available
- Reinforcement learning
  - don't need a human, could just have a fixed goal with a reward
  - still needs constraint when the robot evolves in high-dimentional and large spaces.

*These methods are good an'all but we want more autonomous generalized learning.*

##### 1.2 Driving autonomous exploration
- **Active learning algorithms** can be can be considered as organized and constrained **self-exploration process**.

- In **regression**, **active-learning algorithms** are used to learn mapping between **input space X** and **output space Y** while minimizing the sample comoplexity.
	- All this is done with minmal number of examples necessary to reach a fiven performance level.

- Examples of **critirea** for that can be used to evaluate the utility of given sampling candidates:
	- Maximazation of prediction errors
	- Local density of already queried points
	- maximization of the decrease of global model variance
	- expected improvement
	- maximal uncertainty of the model
	- *(All these methods have papers backing them, and there are more)*

- There are active-extentions to most of the existent learning methods:
	- logistic regression
	- support vector machine
	- gaussian process

#### 1. Motor learning and exploration of forward and inverse models
##### 1.1 Constraining the exploration
##### 1.2 Driving autonomous exploration
##### 1.3 Driving the exploration at a higher level
#### 2. Competence based intrinsic motivation: the self-adaptive goal generation RIAC architecture
##### 2.1 Global architecture
##### 2.2 Model formalization
##### 2.3 Lower time scale: active goal directed exploration and learning
##### 2.4 Higher scale: goal self-generation and self-selection
###### 2.4.1 Measure of competence for a terminal reaching attempt
###### 2.4.2 Measure of competence during a reaching attempt or during goal-directed optimization
###### 2.4.3 Definition of local competence progress
###### 2.4.4 Goal self-generation using the measure of interest
###### 2.4.5 Reductionb of the initiation set
##### 2.5 New challenges of unknown limits of the task space
##### 2.6 Pseudo-code
#### 3. Experimental setup 1: learning inverse kinematics with a redundant arm
##### 3.1 Control paradigms for learning inverse kinematics
##### 3.2 Representation of forward and inverse models to be learnt
##### 3.3 Robotic setup
##### 3.4 Evaluation of competence
##### 3.5 Addition subgoals
##### 3.6 Active goal directed eploration and learning
###### 3.6.1 Reaching phase
###### 3.6.2 Exploration phase
##### 3.7 Qualitative results for a 15 DOF simulated arm
###### 3.7.1 Evolution of competences over time
###### 3.7.2 Global exploration over time
###### 3.7.3 Exploration over time inside reachable areas
###### 3.7.4 Emergent process
###### 3.7.5 Robustness in high-volume task spaces
###### 3.7.6 Conclusion of qualitative results
##### 3.8 Quantitaive results for experiments with taask spaces of different sizes
###### 3.8.1 Exploration in the reachable space
###### 3.8.2 Robustness in large task spaces
##### 3.9 Qualitative results for experiments with an arm of different numbers of DOF and geometries
###### 3.9.1 Qualitative results
###### 3.9.2 Conclusion of quantitative results
##### 3.10 Qualitative results for a real 8 DOF arm
#### 4. Experimental setup 2: learning omnidirectional quadruped locomotion with motor synergies
##### 4.1 Formalization
##### 4.2 Robotic setup
##### 4.3 Measure of competence
##### 4.4 Active goal directed exploration and learning
###### 4.4.1 Reaching phase
###### 4.4.2 Exploratopm åhase
##### 4.5 Qualitative results
##### 4.6 Quantitative results
##### 4.7 Conclusion of results for the quadruped experiment
#### 5. Experimental setup 3: learning to control a fishing rod with motor synergie
##### 5.1 Robotic setup
##### 5.2 Qualitative results
##### 5.3 Quantitaive results
#### 6. Conclusion and future work
