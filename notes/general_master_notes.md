
### Text String
	- Intelligen adaptive curiosity a source of self-development **(IAC)**
	- Active learning of inverse models with intrinsically motivated goal exploration in robots. **(SAGG-RIAC)**
	- Goal-Directed Learning of Hand-Eye Coordination in a Humanoid Robot

### Additional reading material:
	- **Active learning of inverse models with intrinsically motivated goal exploration in robots. **(SAGG-RIAC)****
		- SSA algorithm [14]
		- RIAC [51]
		- what is intrinsic motivation [63]
