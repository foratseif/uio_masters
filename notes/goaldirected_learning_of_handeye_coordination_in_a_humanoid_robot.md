<!-- math equations -->
[matheq1]: https://latex.codecogs.com/gif.latex?f%3AQ%5Crightarrow%20X
[matheq2]: https://latex.codecogs.com/gif.latex?f%3AX%5Crightarrow%20Q

## Goal-Directed Learning of Hand-Eye Coordination in a Humanoid Robot

***Uses goal-babbling for visuo-motor coordination of ALdebaran Nao, and also uses it to coordinate head and arms movements.***

### Useful remarks
- This text could be stringed with:
	- Intelligen adaptive curiosity a source of self-development **(IAC)**
	- Active learning of inverse models with intrinsically motivated goal exploration in robots. **(SAGG-RIAC)**
	- Goal-Directed Learning of Hand-Eye Coordination in a Humanoid Robot

- (*So maybe connect theese works when writing about them*)

### Abstract
- Visuo-motor coordination is known to be the most important factor for development of broader range of cognative and motor skills.
- Article investigaates how a recent concept, named *goal babbling*, relates to a visuo-motor coordination task in the humanoid robot Aldebaran Nao.
- Aldebbaran Nao requires 2 subsystems of motors
    - head
    - motor arms

#### Goal babbling:
- Builds on the findings in develeopment phsycology showing that infants attempt goal-directed movements early on in their deveopment enabling them to rapidly and effienctly bootstrap their motor system.
- Its shown to be better than random motor babbling for learning kinematics.
    - In perticular bodies with many DOF

#### Findings:
- Goal babbling in good for visuo-motor coordination skills
- Also suggest that goal babbling is particually effective in the case where two seperate motor sub-systems, head and arm, need to be coordinated.

### I INTRO:
- Psycology have reported repeatedly that control of movements and coordination of different parts of the body are a **fundemental** prerequisite for the development of complex motor and cognitive skills in humans.

- Article referres 3 papers that show early human **goal babbling**.

- Sensory-motor coordination is a key ability for moodern robots.
    - Defined as: the ability to achieve desired sensory outcomes by means of an agents own actions.

- A prototypical instance of a sensory-motor coordination in **control theory** is **forward kinematics**/**forward model**.
    - Its the ability of a robot to control its end-affector in 2D or 3D task space by actions in an m-dimentional motor space.

- To accomplish this we have to:
    - **First:**
		- The robot predicts outcome x of a motor action q .
		- The model is: ![alt text][matheq1] where *Q* and *X* are motor space and task space respectively.
    - **Second:**
		- Infer a motor command *^q* to achieve a desired sensory outcome *x-star* which is the inverse model: ![alt text][matheq1]

- Developmental robotcs asserts that exploration for robots, that aquire the attributes below, is done by generating samples (q,x) of the unknown forward and inverse function through an increamental learning process.
	- real-world
	- high-dimentional
	- redundant
	- possibly non-stationary
- This is done for soft-robots with high non-linear control.


- Motor babbling focuses on exploring the motor space *Q*.
- Motor actions are chosen and successively executed and their sensory outcome is observed.


- In recent publications the authors show:
	- how a humanoid robot can aqcuire hand-eye coordination
	- reaching skills by exploring its movement
- these capabilities were achieved through:
	- random body babbling
	- by using biologically inspired internal model of the robot body.
- Problem with random babbling is:
	- high-dimentional motor space is is hard to explore fully
	- and that a lot of of different actions can map to same outcome

- Solution to this is **goal-babbling** which explores the sensory space X by goal directed exploration. This is done by:
	- The agent agent chooses goals *x-star* from sensory space
	- The agent infers actions *q^* to teach them based on the knowledge acquired (up to that point).
	- The agent updated the inverse and forward kinematics along the way.
This idea is inspired by study done on infants.

#### In This Work:
- The authors implenended and compared different exploration methods on Aldebaran Nao for **the development of readching skills using feedback from one of its cameras**
- These methods were:
	- FILL IN IMPORTANT
	- FILL IN IMPORTANT

- This study used on-board sensory instead of external as in the other studies metioned, these studies user feedback from externally mounted cameras or position information of simulator to generate samples for learning.


- Challanges with Nao's camera as sensor:
	- Visual range is narrow for a fixed camera position. This requires cordination from both the hand and the head.
- The learning **problem** in this case breaks down into 2 concurrent sub-problems:
	1. learning of the inverse kinematics of the arm to control the position of the end effector
	2. learning of the inverse kinematics of the head to be able to generate sensory feedback for the first problem.

- The **solution** proposed is a two-level model architecture, with two different inverse-models
	- one for the arm kinematics
	- one for the head kinematics

- The **tool** used for this is Explauto (python library for sensory learning)

- And the **result** of this:
	- indicate an important role of goal babbling for visuo-motor coordination tasks.
	- illustrate how the two levls of the model relate to each other
	- how a **synergic relationship** emerges between the hand arm exploration when the robot eplores in a curisity driven manner.

### II METHODS:

**A. Robotic Setup:**
- 5 joints in each of its arm
- 4 joints is moved
	- shoulder-roll
	- shoulder-pitch
	- elbow-roll
	- elbow-yaw
- 2 HD cameras positioned on its chin and forehead
- 2 neck joints were moved
	- head-pitch
	- head-yaw
- Lov-level built-in function to move the joints in absolute position.
- Sensory information c **WRITE MORE NOTEEEEEES**

**B. Model Architecture**
- Nao's task is to learn through exploration the forward and inverse relation between the joints of its arms and the position of its end effector.


