## Resilient Machines Through Continous Self-Modeling

***Describes a robot that can recover from injury autonomously, through continuous self-modeling.***

A four legged machine uses actuation-sensation relationships to directly infer its own structure. It uses this self-model to generate forward locomotion. The self-model adapts when a leg is removed leading to the generation of alternative gaits (a persons manner of walking).

The goal is to achieve robust performance under uncertainty. (No repatativ tasks in structured environments).

Most robot systems use manually constructed mathematical models that capture the robots dynamics (fowrad and inverse dynamics), theese models are then used to plan actions.

Making accurate models difficult for complex machines (even though , escpecially when accounting for topological changes. Even though some parametric identification method exist for autimatically improving these models (check source 10-12).

Much is done for learning robots to model their environment, but not much for making robots learn their own morphology.

Unlike other approaches to damage recovery, the concept introduced here does not presuppose:
- built-in redundancy (18, 19)
- dedicated sensor arrays
- contengency plans designed for anticapated failures (20).


### Main algorithm:
Our approach is based on the concept of multiple competing internal models and heneration of actions to maximize disagreement between predictions of these models.
Basically the actions done by the robot should be done in such a way that would seperate the different models in order to highlight the right one.

- The robot performs an arbitrary motor action and records the resulting sensory data.
- The model component synthesis component then synthesizes a set of 15 candidate self-models using stochastic optimization to explain the observed sensory-actuation casual relationships.
- The action synthesis component then uses these models to find a new action most likely to elicit the most information from the robot.
- This is accomplished by searching for the actuation pattern that, when executed on each of the candidate self-models, cause the most disagreement across the predicted sensor signals (21-24).
- This new action is performed by the physical robot, and the model synthesis component now reiterates with more available information for assessing model quality.
- After 16 cycles of this process have terminated, the most accurate model is used by the behaviour synthesis component to create a desired behaviour that can then be executed by the robot.
- If the robot detects unexpected sensor-motor pttern or an external signal as a result of unanticipated morphological change, the robot reinitiates the alternating cycle of modeling and exploratory actions to produce new models reflecting the change.
- The new most accurate model is now used to generate a new, compensating behavious to recover functionality.

<!-- ### Three algorithms: -->
<!-- #### Modeling: -->
<!-- #### Testing: -->
<!-- #### Prediction: -->

### Actual Robot:
The procposed provess was tested on a four-legged physical robot that had:
- eight motorized joints
- eight joint angle sensors
- two tilt sensors

After the damage was done, the space of topologies is fixed to the previously inferred morphology, but the size of the limbs can be scaled.

### The experiment:
The proposed process was compared to 2 baseline algorithms that use random data acquision (our process uses self-model-driven data aquision.
- **Baseline 1:**
	- 16 random actions were executed by the physical robot and the resulting data were supplied to the model synthesis component for the batch training **(DAFUQ IS BATCH TRANING?)**.
- **Baseline 2:**
	- Rather than search for the action that creates disagreement among competing candidate self-models, this methods action shynthesis component output a random action. We still cycled through the different self-models, but we still output a random model.

30 experiments of each of the 3 algorithm variants were conducted, both before and after the robot suffered damage.
When the damage was applied to the robot the robot began with the best model produced by the model-driven algorithm.

### Results:
The model-driven algorthim was more accurate with generating models that are topologically correct. And the final models were more accurateon average. (these conclusions apply both before and after damage).

**Important:** This indicates that alternating random actions with modeling compared with simply performing several actions first and then modeling, does not improve model synthesis (baseline 2 does not outperform baseline 1), but a robot that actively chooses which action to perform next on the basis of its current set of hypothezied self-models has a better change of successfully inferring its own morphology than a robot that acts randomly (the model-driven algorithm outperforms baseline algorithms 1 and 2).

The robot does now know its morphology so the approach for determening the accuracy of the self-model is measuring the disagreement in the model-set. This is because there is a positive correlation between **model disagreement** and **model error**, across the 30 experiments that use the model-driven algorithm. (Spearman rank correlation = 0.425, P < 0.02).
Therefore the expermients with the most model agreement -> most successful among the 30 experments.

### Discussion:
This experiments builds(?) on the self-modeling suggested in source 26 by showing for the first time that a physical robot is  able to autonomously recover its own topology with little priori knowledge. In addition the robot is able to optimize its parameters after unexpected morphologial change.

Article suggest a bunch of other stuff too.
