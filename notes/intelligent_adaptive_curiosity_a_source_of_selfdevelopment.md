## Intelligent Adaptive Curiosity: a source of Self-Development

***Presents mechanisim for exploration called Intelligent Adaptive Curiosity (IAC)***

#### Abstract
- IAC is a drive which pushes the robot towrads situations in which it maximizes its learning progress by making the robot focus on sitiuations in which the outcome is neither too unperdictable nor too predictable.
- This is a mechanism for self-development
- The complexity increases with the robots learning (autonamously)
- (might have difficulty still in easy situations)
- (avoids situations where nothing can be learnt)

### 1. An engine for self development
- We need the activities to increase in complexity as the agent increases in capabilities.
- Several studies have showed that you can increase the efficiency of the learning of a robot by having the robot do tasks of increasing diffuculty (by putting it in situations in which the task to be solved was of increasing difficulty.)
- The problem with the approach in the studies is that the humans had to manually adjust the complexity of the task/situation. (**passive development**)

- This paper presents the mechanisiim for **self-delvelopment** (**active learning**).
- This means that the robot is put in complex, continious, dynamic environment and is able to figure out without apriori knowledge which situations in the environment have complexity which is *suited for efficient learning* at a given moment of its development.

- This algorthim is related to certain concepts mentioned in its sources. And the jist of what it does is:
	- It tags all situations as positive or negative using real numbers.
	- Theese tags are set based on the learning progress potential for theese situations.
	- This makes the robot act so that it finds itself often in positive situations actions, wgucg brings enternal rewards.

#### Intelligent Adaptive Curiosity (IAC):
- ***Drive:*** Not about maintinance of physical variable (staying warm or not starve) but rather **the learnig progress** (which is an abstact congnative dynamic variable) that has to be maximized at all times.
- ***Curiosity:*** It pushes the robot towards novel situations in which things can be learnt.
- ***Adaptive:*** The desired situation's complexity changes over time
- ***intelligent:*** It keeps the robot away both from situatiomns which are too predictable and from sitautions that are too unpredictable.

### 2. Implementing Intelligent Adaptive Curiosity

#### 2.1 Operationalzing the concept of learning progres

- **Prediction machine P** allows the robot to predict **what is going to happen (S(t+1))** after doing **action A(t)** in the**sensory-motor centext SM(t)**
	- P(A(t), SM(t)) = S(t+1)
- After prediction the robot does the action then measures the **actual concequence Sa(t+1)** and computes the **actual concequence E(t) = |S(t+1) - Sa(t+1)|**
- As time goes the robot gets a **list of successive errors LE = E(0), E(1), E(2), E(3), ..., E(t)**.
- This list is used to measure the evolution of the mean error in prediction.
	- Done using a **sliding window** which computes the **mean of the last DELAY errors**.
		- DELAY = 150 in the setup described in the paper
- This gives a new list **LEm = Em(DELAY), Em(DELAY+1), Em(DELAY+2), ..., Em(t)**
	- where **Em(t) = mean( E(t), E(t-1), E(t-2), ..., E(t-DELAY) )**
- We can then propose a definition of the learning progress at time **t** as:
	- **LP(t) = -(Em(t) - Em(t-DELAY))**
- In order to predict **learning progress** of a situation at time **t+1**:
	- robot predicts **prediction error: E(t+1)**
	- using that it computes **mean error of DELAY error: Em(t+1)**
	- then it calculates **learning rate: EP(t+1)**
- In doing this we end up with a **meta-learning machine MP** which learns to predict the errors of **P**.
	- MP(A(t), SM(t)) = Ep(t+1)
	- (The learning is done when it gets the actual error E(t+1))



- **The algorithm:**
	- At each timestep t, for choosing the action to take:
		- make list of possible actions
		- evaluate each of them in terms of the learning progress it may bring (for the evaluation of each action Ai(t)):
			- Compute MP(Ai(t), SM(t))=Epi(t)
			- Compute associated learning progress LPi(t)
			- Choose the action for which the predicted learning progress is maximal
		- if the space of action is continous
			- then it is sampled randomly



- Sometimes a random action is chooses, this allows the algorithm to discover new niches of learning progress.
- The current algorithm is used in the 2003 source of this paper, it did not work as desired.
	- The robot would find a behaviour which will provide high value of LP(t) but will be very inefficient in terms of learning, and will not provide the ability to develop.
	- This happens when the robot alternates between very predictable situations to very unpredictable situations, going from unpredictable to predictable will cause the learning rate to drop thus is evaluated as learning progress.
- The **proposed solution** to this problem is comparing the mean error in prediction between situations which are similar, rather than predicting between situations which are successive in time.
	- The P machine of the robot is now composed by a set of **experts** which are specialized in particual zones of the sensory-motor space. (particular kinds of situations)
		- Each **expert** possesses a **set of training examples**, and each **training example** is posessed by only **one expert**.
		- For each **expert** the **example set** is used to make predictions using (in this case) k-nearest-neighbors
	- We start with only **one expert** and a **criterion C1** that determines whether it should be split into **two experts**
		- In this example (for simplicity): **C1** will split when the number f examples is above a threshold set to **NS** (=250)
		- **C2** decides how the expert is split into two experts. This criterion finds a dimention and a value, to cut, to minimize the variance of the examples while trying keeping the size of the two sets relativaly balanced.
		- When a prediction has made given A(t) and SM(t) one first determines which is the expert which is a specialist for the zone of the sensory-motor space which contains this point. This is done by using the successive cut values stored by the expert.
		- Each expert *i* keeps track of the error and computes the mean error in prediction in the last DELAY examples. **Empi(t)**
			- **LPi(t) = -(Empi(t) - Empi(t-DELAY))**

#### 2.2 Evaluating the algorithm in a simulated robotic set-up

- Simulation used: Webots 2004
- Robot is a box with wheels that can be controlled by setting speed to between 1 and -1.
- Robot can emit a sound.
- Action space is 3-dimentional and continous
	- left-speed
	- right-speed
	- sound frequency
- Robot moves in square room with walls
- Toy in room that can also move
	- moves randomly if sound emitted is in frequency f1=[0;0.33]
	- stops moving if frequency f2=[0,34;0,66]
	- jumps into the robot if frequency f3=[0,67;1]
- Robot percieved distance to toy and walls with infrared sensors
- Prediction machine P treis to predict the next distance to the toy given the current sensory-motor context and an action to take.
- The robot uses IAC, the robot has no prior knowledge and does not know difference between setting wheel speed or sound frequency.
- It does not know that there exist three zones of the sensory-motor space of different complexities:
	- f1: distance to toy is random
	- f2: distance is predictable
	- f3: distance is close to 0 and is predictable and easy to learn

### Results:
- The robot manages to autonoumasly discover these three zones, evaluate their relative complexity, then explot this information for organizing its own behaviour.
- There was three groups of experts that were formed.
	- All groups was self-coherant
	- one group correspoonded to situations in sound frequency f1
	- one group with sound in f2
	- one group with sound in f3
	- The first split happened between f3 and rest
		- f3 were initially the most important source of learning, but were quickly ignored because they were easy to learn.
	- Then split between situations in f2 and f1 happened.
		- this happened because of random action while exploring situations in f3
	- The robot then realizes that situations in f2 provide best learning progress while f1 is chaotic.
	- Robot focues on exploring f2 after finishing f3.
		- variying motor speeds inside f2 to see if it can predict outcome

