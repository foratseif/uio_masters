- During the recent DARPA Robotics Challenge (2015), manyrobots had to be ‘‘rescued’’
  by humans because of hardware fail-ures
    [ C.Atkeson,etal.,Nofalls,noresets:ReliablehumanoidbehaviorintheDARPAroboticschallenge,in:Proc.ofHumanoids,2015,pp.623–630.]
